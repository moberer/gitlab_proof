This is a Keyoxide identity proof.
Keyoxide is a tool to verify online identities across accounts.
Visit https://keyoxide.org/798e7c4afbe303f95f14b9cc65062e9d59a20cf4 to verify my linked accounts.

(Also make sure that the keyoxide page links back to here or that the link matches the hex in the project description!)
